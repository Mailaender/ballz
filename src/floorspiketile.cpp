/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "floorspikestile.hpp"
#include "ball.hpp"
#include "unused.hpp"

FloorSpikeTile::FloorSpikeTile(int x, int y, const std::string& filename)
:HostileTile(x, y, filename),
mState(DOWN),
mFrame(0),
mTime(0),
mSolid(false),
mHostile(false)
{

}

FloorSpikeTile::FloorSpikeTile(int x, int y, BITMAP* animation)
:HostileTile(x, y, animation),
mState(DOWN),
mFrame(0),
mTime(-1),
mSolid(false),
mHostile(false)
{

}

FloorSpikeTile::~FloorSpikeTile()
{

}

bool FloorSpikeTile::isSolid()
{
    return mSolid;
}

void FloorSpikeTile::logic()
{
    mTime++;

    if (mState == DOWN)
    {
        if (mTime >= 203)
        {
            mFrame = 0;
            mTime = 0;
        }

        if (mTime == 100)
        {
            mState = MOVING_UP;
            mFrame = 1;
            mHostile = true;
        }

        return;
    }

    if (mState == MOVING_UP)
    {
        if (mTime == 101)
        {
            mFrame = 2;
            mSolid = true;
        }
        if (mTime == 102)
        {
            mState = UP;
            mFrame = 3;    
        }

          return;
    }

    if (mState == UP)
    {
        if (mTime == 202)
        {
            mState = MOVING_DOWN;
            mFrame = 2;
        }

          return;
    }

    if (mState == MOVING_DOWN)
    {
        if (mTime == 203)
        {
            mFrame = 1;
            mSolid = false;
        }

        if (mTime == 204)
        {
            mState = DOWN;
            mFrame = 0;
            mTime = -1;
            mHostile = false;
        }
    }
}

void FloorSpikeTile::draw(BITMAP *dest, int UNUSED(frame), int scroll)
{
	masked_blit(animation, dest, mFrame*16, 0, getX() - scroll, getY(), 16, 16);
}

void FloorSpikeTile::collision(Level* UNUSED(level), Ball* ball)
{
    if (mHostile)
    {
		int bx = ball->getCenterX();
		int by = ball->getCenterY();
        if (by < getY() || (bx - bx % 16 == getX() &&  by - by % 16 == getY()))
        {
            ball->kill();
        }
    }
}
