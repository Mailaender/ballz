/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <allegro.h>
#include <string>
#include <iostream>
#include <exception>

#include "game.hpp"
#include "guichan.hpp"

// Handle window manager close button
// Cf. http://www.allegro.cc/manual/4/api/using-allegro/set_close_button_callback
// and the 'main' function below.
Game* cur_game = NULL;
void close_button_handler(void)
{
  if (cur_game != NULL)
    cur_game->quit();
}
END_OF_FUNCTION(close_button_handler)

int main(void)
{
	try
	{
		Game game;
		cur_game = &game;
		LOCK_FUNCTION(close_button_handler);
		set_close_button_callback(close_button_handler);
		game.run();
	}
    catch (gcn::Exception e)
    {
        std::cerr << e.getMessage();
        allegro_message("%s", e.getMessage().c_str());
        return 1;   
    }
	catch (std::string e)
	{
		std::cerr << e;
        allegro_message("%s", e.c_str());
		return 1;
	}

	return 0;
}
END_OF_MAIN()
