/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BALLZ_LEVEL_HPP
#define BALLZ_LEVEL_HPP

#include <string>
#include <list>
#include <allegro.h>
#include "tilemap.hpp"
#include "ball.hpp"
#include "guichan.hpp"
#include "guichan/allegro.hpp"
#include "gui/datawriter.hpp"
#include "background.hpp"

class Level
{
public:
    Level();
    ~Level();
    void load(const std::string& filename);
    void draw(BITMAP* buffer);
    void logic();
    bool isCompleted();
	bool isFailed();
    int getCollectedStars() { return mCollectedStars; }
    void setCollectedStars(int collectedStars) { mCollectedStars = collectedStars; } 
    void setGoalReached(bool goalReached) { mGoalReached = goalReached; }
    int getTime() { return mTime; }

protected:
    void gameLogic();
	void introLogic();
    void handleCollision();

    TileMap* mTileMap;
	int mFrame;
    Ball* mBall;
    int mScroll;
    int mCollectedStars;
    bool mGoalReached;
    bool mCompleted;
    int mTime;

    gcn::Gui* mGui;
    gcn::AllegroGraphics* mAllegroGraphics;
    gcn::AllegroImageLoader* mAllegroImageLoader;

    gcn::Container* mTop;
    gcn::Label* mStarsLabel;
    gcn::Label* mTimeLabel;
    gcn::Label* mReady;
    gcn::Label* mSteady;
    gcn::Label* mRoll;
	DataWriter* mDataWriter;
	std::list<std::string> mIntroText;
	bool mLastEnter;
	SAMPLE *mBeepSample;

    Background* mBackground;
};

#endif
