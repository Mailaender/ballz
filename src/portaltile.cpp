/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "portaltile.hpp"
#include "level.hpp"
#include "unused.hpp"

PortalTile::PortalTile(int x, int y, const std::string& filename)
: AnimatedTile(x, y, filename),
  mBallIsTeleported(false),
  mStillCollision(false)
{

}

PortalTile::~PortalTile()
{

}

void PortalTile::collision(Level* UNUSED(level), Ball* ball)
{
    if (!mBallIsTeleported)
    {
		int dest = ball->getCenterX() - mDestinationPortal->getX() - 8;
		if (dest < 0) {
			dest = -dest;
		}
		if (dest < 100) {
			dest = 100;
		}
		ball->freeze(dest / 5);
        ball->setPosition(mDestinationPortal->getX(), 
                          mDestinationPortal->getY());
		
        mDestinationPortal->setBallIsTeleported(true);
    }
    else
    {
        mStillCollision = true;
    }
}

void PortalTile::logic()
{
    if (!mStillCollision) 
    {
        mBallIsTeleported = false;
    }

    mStillCollision = false;
}
