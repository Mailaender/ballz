/*
 * Copyright (c) 2007, Olof Naessen and Per Larsson
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Darkbits nor the names of its contributors may be 
 *      used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <math.h>

#include "desertbackground.hpp"
#include "resourcehandler.hpp"

DesertBackground::DesertBackground()
{   
    mDesertBackground0 = ResourceHandler::getInstance()->getBitmap("desertbackground0.bmp");
    mDesertBackground1 = ResourceHandler::getInstance()->getBitmap("desertbackground1.bmp");
    mDesertBackground2 = ResourceHandler::getInstance()->getBitmap("desertbackground2.bmp");
    mDesertBackground3 = ResourceHandler::getInstance()->getBitmap("desertbackground3.bmp");
	mFrame = 0;
}

void DesertBackground::draw(BITMAP* dest, int scroll)
{
    draw_sprite(dest, mDesertBackground0, -((scroll / 5) % 320), 0);
    draw_sprite(dest, mDesertBackground0, -((scroll / 5) % 320) + 320, 0);

    draw_sprite(dest, mDesertBackground1, -((scroll / 4) % 320), 0);
    draw_sprite(dest, mDesertBackground1, -((scroll / 4) % 320) + 320, 0);

    draw_sprite(dest, mDesertBackground2, -((scroll / 3) % 320), 0);
    draw_sprite(dest, mDesertBackground2, -((scroll / 3) % 320) + 320, 0);

	drawSmoke(dest, scroll);

    draw_sprite(dest, mDesertBackground3, -((scroll / 2) % 320), 0);
    draw_sprite(dest, mDesertBackground3, -((scroll / 2) % 320) + 320, 0);
}

void DesertBackground::drawSmoke(BITMAP* dest, int scroll)
{
	std::list<Particle>::iterator it = particles.begin();
	while (it != particles.end()) {
		int r = it->ttl / 2 > 10 ? 10 : it->ttl / 2;
		if (r > 0) {
			int x = ((int)it->x - (scroll * 2) / 3 + 100500) % 600;
			circlefill(dest, x, it->y, r, makecol(120, 100, 80));
			circlefill(dest, x - r / 3, it->y - r / 3, (r * 3) / 4, makecol(150, 130, 110));			
		}
		it++;
	}
}

void DesertBackground::logic()
{
	if (mFrame++ % 2) {
		float x = rand() % 10 + 70;
		float y = 190.0;
		float dx = (rand() % 50) / 100.0 + 0.5;
		float dy = -1.0 - (rand() % 100) / 100.0;
		int ttl = 40 + rand() % 30;
		particles.push_back(Particle(x, y, dx, dy, ttl));
	}

	if (particles.empty()) {
		return;
	}

	std::list<Particle>::iterator it = particles.begin();
	while (it != particles.end()) {
		it->x += it->dx + rand() % 3 - 1;
		it->y += it->dy + rand() % 3 - 1;
		it->ttl--;
		it++;
	}

	while (particles.begin()->ttl <= 0) {
		particles.erase(particles.begin());
	}

}
